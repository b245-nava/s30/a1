// Count total of fruits on sale

db.fruits.aggregate([
	{ $match: {onSale: true}},
	{ $count: "fruitsOnSale"}
	]);

// Count of fruits with stock more than or equal to 20

db.fruits.aggregate([
	{ $match: {onSale: true, stock: {$gte: 20}}},
	{ $count: "enoughStock"}
	]);

// Average price of fruits onSale per supplier

db.fruits.aggregate([
	{ $match: {onSale: true}},
	{ $group: { _id: "$supplier_id", avgPrice: {$avg: "$price"}}}	
	]);

// Highest price of fruit per supplier

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", maxPrice: {$max: "$price"}}}
]);

// Lowest price of fruit per supplier

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", minPrice: {$min: "$price"}}}
]);